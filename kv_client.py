import os
from cassandra import cluster
from cassandra import auth
from cassandra.query import dict_factory
from cassandra.cqlengine import models
from cassandra.cqlengine import columns
from cassandra.cqlengine import connection
from cassandra.cqlengine import query
from cassandra.cqlengine import management

import logging

class KeyValueStoreClient:
    KEYSPACE_NAME = "kvstore"

    instance = None

    @classmethod
    def get_instance(cls):
        instance = cls.instance
        if instance is None:
            instance = KeyValueStoreClient()
            cls.instance = instance
        return instance

    def __init__(self):
        cassandra_hosts = os.getenv('KV_DB_HOSTS').split(',')
        cassandra_user = os.getenv('KV_DB_USER')
        cassandra_password = os.getenv('KV_DB_PASSWORD')
        logging.info("hosts: " +str(cassandra_hosts))

        auth_provider = auth.PlainTextAuthProvider(username=cassandra_user, password=cassandra_password)
        self.cluster = cluster.Cluster(cassandra_hosts, auth_provider=auth_provider)
        self.session = self.cluster.connect()
        self.session.row_factory = dict_factory
        connection.set_session(self.session)
        self.session.set_keyspace(self.KEYSPACE_NAME)
        self.KeyValue.__keyspace__=self.KEYSPACE_NAME
        management.sync_table(self.KeyValue)

    def get_session(self):
        return self.session

    def get_value(self, instance_id, scope_id, key):
        try:
            kv = self.KeyValue.get(instance_id=instance_id, scope_id=scope_id, key=key)
            return kv.value
        except query.DoesNotExist as e:
            return None

    def set_value(self, instance_id, scope_id, key, value):
        session = self.get_session()
        try:
            kv = self.KeyValue(instance_id=instance_id, scope_id=scope_id, key=key)
            kv.value = value
            kv.save()
        except Exception as e:
            logging.error(str(e))
            raise

    def delete_value(self, instance_id, scope_id, key):
        session = self.get_session()
        try:
            kv = self.KeyValue(instance_id=instance_id, scope_id=scope_id, key=key)
            kv.delete()
        except query.DoesNotExist as e:
            logging.info(str(e))
            return None

    class KeyValue(models.Model):
        instance_id = columns.Text(primary_key=True)
        scope_id = columns.Text(primary_key=True)
        key = columns.Text(primary_key=True)
        value = columns.Text()
