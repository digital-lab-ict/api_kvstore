import io
import os
import json
import logging
import urllib2
import time

from flask import Flask, request, abort
from flask_cors import CORS

from jose import jwt

import kv_client

logging.basicConfig(level=logging.INFO)

app = Flask(__name__)
CORS(app)

def decode_token(token):
    return jwt.get_unverified_claims(token)

def get_instance_id(token_payload):
    return token_payload.get("app_id")

def get_scope_id(scope, token_payload):
    scope_id = None
    if scope == "general":
        scope_id = scope
    elif scope == "user":
        scope_id = token_payload.get("user_id")
    else:
        raise Exception("invalid scope")
    return scope_id

@app.route("/")
def hello():
    return "api server v1.0"

@app.route("/persistence/1.0/kvstore/<scope>/<key>", methods=['GET'])
def kvstore_read(scope, key):
    logging.info("kvstore_read.1")
    ret_val = json.dumps({"status": "ko", 'error': 'invalid_request'})
    try:
        token_payload = decode_token(request.headers.get('Authorization'))
        instance_id = get_instance_id(token_payload)
        scope_id = get_scope_id(scope, token_payload)

        client = kv_client.KeyValueStoreClient.get_instance()
        value = client.get_value(instance_id, scope_id, key)
        if value is None:
            abort(404)
        ret_val = value
    except Exception as e:
        logging.info("Exception: " + str(e))
    return ret_val

@app.route("/persistence/1.0/kvstore/<scope>/<key>", methods=['PUT'])
def kvstore_write(scope, key):
    logging.info("kvstore_write.1")
    ret_val = {"status": "ko", 'error': 'invalid_request'}
    try:
        token_payload = decode_token(request.headers.get('Authorization'))
        instance_id = get_instance_id(token_payload)
        scope_id = get_scope_id(scope, token_payload)

        client = kv_client.KeyValueStoreClient.get_instance()
        client.set_value(instance_id, scope_id, key, request.get_data())
        ret_val = {"status": "ok"}
    except Exception as e:
        logging.info("Exception: " + str(e))
    return json.dumps(ret_val)

@app.route("/persistence/1.0/kvstore/<scope>/<key>", methods=['DELETE'])
def kvstore_delete(scope, key):
    logging.info("kvstore_write.1")
    ret_val = {"status": "ko", 'error': 'invalid_request'}
    try:
        token_payload = decode_token(request.headers.get('Authorization'))
        instance_id = get_instance_id(token_payload)
        scope_id = get_scope_id(scope, token_payload)

        client = kv_client.KeyValueStoreClient.get_instance()
        client.delete_value(instance_id, scope_id, key)
        ret_val = {"status": "ok"}
    except Exception as e:
        logging.info("Exception: " + str(e))
    return json.dumps(ret_val)

@app.route("/swagger")
def test():
  return app.send_static_file('api_kvstore_swagger.yaml')

if __name__ == "__main__":
    kv_client.KeyValueStoreClient.get_instance()
    app.run(host='0.0.0.0',port=5000,threaded=True)
